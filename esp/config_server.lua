local M = {}

local config
local srv

local function change_offset(new_offset)
    if new_offset ~= nil then
        config.change_offset(tonumber(new_offset))
    end
end 

local function color_no_nil_member(color)
    return color.red ~= nil and color.green ~= nil and color.blue ~=nil and color.alpha ~=nil
end

local function color_string_to_number(color)
    local new_color={}
    new_color.red=tonumber(color.red)
    new_color.green=tonumber(color.green)
    new_color.blue=tonumber(color.blue)
    new_color.alpha=tonumber(color.alpha)
    return new_color
end

local function change_on_color(color)
    new_color= color_string_to_number(color)
    if color_no_nil_member(color) then
        config.change_on_color(color)
    end
end

local function change_off_color(color)
    new_color= color_string_to_number(color)
    if color_no_nil_member(color) then
        config.change_off_color(color)
    end
end

local function sendPage(conn)
   conn:send('HTTP/1.1 200 OK\n\n')
   conn:send('<html>')
   conn:send('<title>Gouge</title>')
   conn:send('<body><h1>Gouge configuration</h1>')
   conn:send('Current offset to UTC: ' .. tostring(config.offset()))
   conn:send('<br>')
   conn:send('<form action="/" method="POST">')
   conn:send('new offset: <input type="text" name="data" value="'..config.offset()..'"><br>')

   conn:send('On color: red<input type="number" name="on_color_red" value="'.. config.on_color().red ..'">')
   conn:send('green<input type="number" name="on_color_green" value="'.. config.on_color().green ..'">')
   conn:send('blue<input type="number" name="on_color_blue" value="'.. config.on_color().blue ..'">')
   conn:send('alpha<input type="number" name="on_color_alpha" value="'.. config.on_color().alpha ..'"><br>')

   conn:send('Off color: red<input type="number" name="off_color_red" value="'.. config.off_color().red ..'">')
   conn:send('green<input type="number" name="off_color_green" value="'.. config.off_color().green ..'">')
   conn:send('blue<input type="number" name="off_color_blue" value="'.. config.off_color().blue ..'">')
   conn:send('alpha<input type="number" name="off_color_alpha" value="'.. config.off_color().alpha ..'"><br>')

   conn:send('<input type="submit">')
   conn:send('</form>')
   conn:send('</body></html>')
end

local function extract_color_from_payload(color_text, payload)
    color = {}
    color.red=string.match(payload,color_text.."_red=(%d+)")
    color.green=string.match(payload,color_text.."_green=(%d+)")
    color.blue=string.match(payload,color_text.."_blue=(%d+)")
    color.alpha=string.match(payload,color_text.."_alpha=(%d+)")

    return color
end

local function start()
    srv:listen(80,function(conn)
       conn:on("receive", 
        function(conn,payload)
            if (string.find(payload, "GET / HTTP/1.1") ~= nil) then
                print("GET received")
                sendPage(conn)
            else
                offset=string.match(payload,"data=(%d+)")
                change_offset(offset)

                on_color = extract_color_from_payload("on_color", payload)
                change_on_color(on_color)

                off_color = extract_color_from_payload("off_color", payload)
                change_off_color(off_color)
                sendPage(conn)
            end
       end)
       conn:on("sent", function(conn)
          conn:close()
          print("Connection closed")
       end)
    end)  

end

function M.start(configuration)
    config = configuration
    srv = net.createServer(net.TCP)
    start()
end

return M
