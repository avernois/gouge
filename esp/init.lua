local pin_clock = 2
local pin_data  = 1

local clock = require('wordclock')
clock.setup(pin_data, pin_clock)

local config = require('config')
config.init("config.sav")
local config_server = require('config_server')

local time = require('time')
time.autosync()
local main = require('main')
main.start(config, config_server, clock, time)

config = nil
config_server = nil
clock = nil
time = nil

package.loaded['wordclock'] = nil
package.loaded['time'] = nil
package.loaded['main'] = nil
package.loaded['config_server'] = nil
package.loaded['config'] = nil
