local M = {}

local data
local clock
 
local leds =  {
    ["H_1"] = {1, 2},
    ["H_2"] = {3, 4, 5},
    ["H_3"] = {11, 12, 13},
    ["H_4"] = {27, 28, 29, 30},
    ["H_5"] = {17, 18, 19},
    ["H_6"] = {9, 10},
    ["H_7"] = {22, 23, 24},
    ["H_8"] = {20, 21},
    ["H_9"] = {14, 15, 16},
    ["H_10"] = {25, 26},
    ["H_11"] = {6, 7, 8},
    ["midi"] = {31, 32},
    ["minuit"] = {33, 34, 35},
    ["heure"] = {36, 37, 38},
    ["s"] = {39},
    ["M_5"] = {54, 55, 56},
    ["M_10"] = {60, 61},
    ["quart"] = {57, 58, 59},
    ["M_20"] = {43, 44, 45},
    ["-"] = {53},
    ["demi"] = {49, 50, 51},
    ["e"] = {52},
    ["le"] = {41, 42},
    ["et"] = {40},
    ["moins"] = {46, 47, 48},
    ["D_1"] = {62},
    ["D_2"] = {63},
    ["D_3"] = {64},
    ["D_4"] = {65}
}


local hours= {
        [0] =  {leds["minuit"]},
        [1] =  {leds["H_1"],  leds["heure"]},
        [2] =  {leds["H_2"],  leds["heure"], leds["s"]},
        [3] =  {leds["H_3"],  leds["heure"], leds["s"]},
        [4] =  {leds["H_4"],  leds["heure"], leds["s"]},
        [5] =  {leds["H_5"],  leds["heure"], leds["s"]},
        [6] =  {leds["H_6"],  leds["heure"], leds["s"]},
        [7] =  {leds["H_7"],  leds["heure"], leds["s"]},
        [8] =  {leds["H_8"],  leds["heure"], leds["s"]},
        [9] =  {leds["H_9"],  leds["heure"], leds["s"]},
        [10] = {leds["H_10"], leds["heure"], leds["s"]},
        [11] = {leds["H_11"], leds["heure"], leds["s"]},
        [12] = {leds["midi"]},
        [13] = {leds["H_1"],  leds["heure"]},
        [14] = {leds["H_2"],  leds["heure"], leds["s"]},
        [15] = {leds["H_3"],  leds["heure"], leds["s"]},
        [16] = {leds["H_4"],  leds["heure"], leds["s"]},
        [17] = {leds["H_5"],  leds["heure"], leds["s"]},
        [18] = {leds["H_6"],  leds["heure"], leds["s"]},
        [19] = {leds["H_7"],  leds["heure"], leds["s"]},
        [20] = {leds["H_8"],  leds["heure"], leds["s"]},
        [21] = {leds["H_9"],  leds["heure"], leds["s"]},
        [22] = {leds["H_10"], leds["heure"], leds["s"]},
        [23] = {leds["H_11"], leds["heure"], leds["s"]}
}

local minutes = {
    [0] = {},
    [5] = {leds["M_5"]},
    [10] = {leds["M_10"]},
    [15] = {leds["et"], leds["quart"]},
    [20] = {leds["M_20"]},
    [25] = {leds["M_20"], leds["-"], leds["M_5"]},
    [30] = {leds["et"], leds["demi"], leds["e"]},
    [35] = {leds["moins"], leds["M_20"], leds["-"], leds["M_5"]},
    [40] = {leds["moins"], leds["M_20"]},
    [45] = {leds["moins"], leds["le"], leds["quart"]},
    [50] = {leds["moins"], leds["M_10"]},
    [55] = {leds["moins"], leds["M_5"]},
    ["demi"] = {leds["et"], leds["demi"]}
}

local dots = {
    [0] = {{}},
    [1] = {leds["D_1"]},
    [2] = {leds["D_1"], leds["D_2"]},
    [3] = {leds["D_1"], leds["D_2"], leds["D_3"]},
    [4] = {leds["D_1"], leds["D_2"], leds["D_3"], leds["D_4"]}
}

local function light(led_status, leds_to_light)
    for i, case in ipairs(leds_to_light) do
        for j, led in ipairs(case) do
            led_status[led]=true
        end
    end

    return led_status
end

local function hour_leds(hour, min)
    rounded_minute = min - min%5

    led_status={}

    my_hour = hour
    if (rounded_minute > 30) then
        my_hour = (hour+1)%24
    end
    
    light(led_status, hours[my_hour])

    if (rounded_minute == 30 and (my_hour==12 or my_hour== 0)) then
        light(led_status, minutes["demi"])
    else
        light(led_status, minutes[rounded_minute])
    end

    light(led_status, dots[min%5])

    return led_status
end

local function word_write(led_status, on_color, off_color)
    local on=string.char(on_color.alpha, on_color.blue, on_color.green, on_color.red)
    local off=string.char(off_color.alpha, off_color.blue, off_color.green, off_color.red)
    st = ""
    for i=1,68 do
        if led_status[i] then
            st=st..on
        else
            st=st..off
        end
    end
    apa102.write(data, clock, st)
end


function M.light_time(hour, minute, on_color, off_color)
    word_write(hour_leds(hour, minute), on_color, off_color)
end

function M.light_dots(nb_dots, on_color, off_color)
    word_write(light({}, dots[nb_dots]), on_color, off_color)
end


function M.setup(data_pin, clock_pin)
    data = data_pin
    clock = clock_pin
end

return M
