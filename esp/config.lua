local M = {}

local config_file = ""
local config={
    time_offset = 0,
    on_color= {
        red=255,
        green=255,
        blue=0,
        alpha=15
    },
    off_color= {
        red=0,
        green=0,
        blue=255,
        alpha=5
    }
}

local function write_config()
    src = file.open(config_file, "w+")
    if src then
        src:write("function load_config()\n")
        src:write("config= {\n")
        src:write("   time_offset="..config.time_offset..",\n")
        src:write("   on_color={")
        src:write("      red="  ..config.on_color.red..",")
        src:write("      green="..config.on_color.green..",")
        src:write("      blue=" ..config.on_color.blue..",")
        src:write("      alpha="..config.on_color.alpha)
        src:write("   },")
        src:write("   off_color={")
        src:write("      red="  ..config.off_color.red..",")
        src:write("      green="..config.off_color.green..",")
        src:write("      blue=" ..config.off_color.blue..",")
        src:write("      alpha="..config.off_color.alpha)
        src:write("   },")
        src:write("}\n")
        src:write("return config\n")
        src:write(" end\n")
        src:close()
    end
end

local function read_config()
    if file.exists(config_file) then
        dofile(config_file)
        config=load_config()
    end
end

function M.offset()
    return config.time_offset
end

function M.change_offset(offset)
    config.time_offset = offset
    write_config()
end

function M.change_on_color(on_color)
    config.on_color=on_color
end


function M.change_off_color(off_color)
    config.off_color=off_color
end

function M.on_color()
    return config.on_color
end

function M.off_color()
    return config.off_color
end

function M.init(configuration_file)
    config_file=configuration_file
    read_config(config_file)
end

return M
