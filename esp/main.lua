local M = {}

local clock
local time
local config
local config_server

local color_noip = { red=255, green=0, blue=0, alpha=5 }
local color_nodns = { red=255, green=0, blue=255, alpha=5 }

local booting_light_phase = 0

local function booting_light(color)
    --print("booting, config", config)
    on_color = config.on_color()
    booting_light_phase = (booting_light_phase +1)%5
    clock.light_dots(booting_light_phase, color, config.off_color())
end

local function update_time()
    now = time.get_zoned_time(config.offset())
    on_color = config.on_color()
    clock.light_time(now["hour"], now["min"], config.on_color(), config.off_color())
end

local function auto_update_time()
    timer = tmr.create()
    timer.alarm(timer, 1000*10, tmr.ALARM_AUTO, update_time)
end

local function wifi_connection(on_connection)
    wifi_connection_light = tmr.create()
    wifi_connection_light:register(500, tmr.ALARM_AUTO, function() booting_light(color_noip) end)
    wifi_connection_light:start()
    
    enduser_setup.start(
        function()
            print("Connecting to wifi", wifi.sta.getip())
            wifi_connection_light:unregister()
            wifi_connection_light=nil
            on_connection()
        end,
        function(err, str)
            print("enduser_setup: Err #" .. err .. ": " .. str)
        end,
        print
    );
end

local function starting_timer()
    booting_timer = tmr.create()
    booting_timer:alarm(500, tmr.ALARM_AUTO, function() booting_light(color_nodns) end)
    time.sync_ntp(function ()
        tmr.create():alarm(30000, tmr.ALARM_SINGLE,
                function()
                    print("Starting config server")
                    config_server.start(config)
                end)
        booting_timer:unregister()
        booting_timer=nil
        update_time()
        auto_update_time()
    end)
end

function M.start(configuration, configuration_server, wordclock, sntptime)
    print("Starting")
    config = configuration
    config_server = configuration_server
    clock = wordclock
    time = sntptime
    wifi_connection(starting_timer)
end

return M
