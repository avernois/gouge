local M = {}

local print = print
local netDnsResolve = net.dns.resolve
local sntpSync = sntp.sync
local tmrCreate, tmr_ALARM_SINGLE, tmr_ALARM_AUTO = tmr.create, tmr.ALARM_SINGLE, tmr.ALARM_AUTO
local rtctimeGet, rtctimeEpoch2cal = rtctime.get, rtctime.epoch2cal

if setfenv then
    setfenv(1, M) -- for 5.1
else
    _ENV =  M -- for 5.2
end

local function single_sync()
    print("Single NTP sync")
    netDnsResolve("0.fr.pool.ntp.org",
        function(sk, ip)
            if (ip ~= nil)
            then
                print("Synchronizing with ", ip)
                sntpSync(ip,
                    function(sec,usec,server)
                        print('sync', sec, usec, server)
                    end,
                    function() end)
            end
        end)
end

function M.sync_ntp(success)
    netDnsResolve("0.fr.pool.ntp.org", 
        function(sk, ip)
            local timer = tmrCreate()
            restart = function () M.sync_ntp(success) end
            if (ip == nil)
            then
                print("DNS fail!, will retry in 1s")
                timer:alarm(1000, tmr_ALARM_SINGLE, restart)
            else
                print(ip)
                sntpSync(ip,
                    function(sec,usec,server)
                        print('sync', sec, usec, server)
                        success()
                    end,
                    function()
                        print("sync failed, will retry in 1s")
                        timer:alarm(1000, tmr_ALARM_SINGLE, restart)
                    end)
            end
        end)
end

local timer = tmrCreate()
timer:register(60*60*1000, tmr_ALARM_AUTO, single_sync)

function M.autosync(delay_ms)
    if delay_ms ~= nil then
        timer:interval(delay_ms)
    end
    timer:start()
end

function M.stop_autosync()
    timer:stop()
end

function M.get_zoned_time(time_shift)
    local timestamp = rtctimeGet()
    return rtctimeEpoch2cal(timestamp + time_shift * 60 * 60)
end

return M