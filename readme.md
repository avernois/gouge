# Gouge: a french word clock

Gouge is a clock that tell time in french. It's made from laser cutted wood, an esp8266, 68 apa102 leds and some other component.

![a word clock](images/gouge-small.jpg)


## case plans

All plans for the cases can be found in the [svg/](svg/) directory. There are three differents parts:
* [front_face_spacer-garuda.svg](svg/front_face_spacer-garuda.svg):
    * they are meant to be cut from 3mm wood (I used birch plywood)
    * front face
        * using Garuda font
        * with french word
    * spacer
        * matching
        * you'll need 2 of it
        * you can glue them together
        * painting the inside in (shiny) white will improve light diffusion
* [box_and_led_holder.svg](svg/box_and_led_holder.svg): 
    * outside edge are to be glued together and with the front face
    * leds are to be glued on the led holder
* [pcb_holder.svg](svg/pcb_holder.svg):
    * this holder has to role: 
        * hold the pcb
        * fill the space so all layers stay in place
    * it's meant for the pcb made from kicad files (in the [kicad/](kicad/) directory)

Note the the viewport is set to an a2 format, so gitlab won't preview the full view, the pcb holder part is mostly not shown (but present in the file :)

## electronics

The project can be found in the kicad directory.


* U2: 1x esp8266 (esp12e in my case) on breakout board and 2 1x8 pin header
* U1: ht7333
* C1: 1x 47µF capacitor
* P1: 1x microusb connector (with a 1x5 pin header)
* P2: 1x3 pin header (used to connect to tx/rx/gnd)
* P3: 1x4 pin header (used to connect led strip)
* SW1: a switch (to connect GPIO0 to ground in order to flash the firmware)

* strips from a 60leds/meter apa102 strip:
    * 8x 8 leds
    * 1x 4 leds

## code

Code can be found in [esp/](esp/). It's in lua and to be run on a [nodemcu firmware](http://nodemcu.readthedocs.io/en/master/).
It requires at least apa102, sntp, rtctime, file, enduser_setup and net modules.

## configuration

### wifi credentials

On the first start, gouge will start a wireless network called SetupGadget_xxxxxx (where xxxxxx is the id of the chip). 
Connect to that network (no credentials required) and navigate to a non secure website (like http://example.com), you'll be redirected to a captive portal where you can enter wifi credential for you own local network.
It should then connect to your network (and shutdown the SetupGadget_xxxxxx network).

### clock configuration

Five second after successfully joined your network, you can access the configuration web page on port 80. It allows you to set the time offset from UTC.

note: time offset is persisted accross reboot.
    
## Next

* add a way to warn user if syncing was not done since a long time
* automatically manage dst
* user config for colors
* make "vingt-cing" be made from "vingt" and "cinq"

## Why Gouge?

 Oh, you mean, project names have to mean something?
 
## Licence

Gouge (software and plans) are under MIT Licence.
